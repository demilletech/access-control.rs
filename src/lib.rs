#![warn(clippy::all)]
#![cfg_attr(feature = "async", feature(async_await))]

pub mod data;
