#[cfg(feature = "async")]
use async_trait::async_trait;
#[cfg(feature = "async")]
use std::future::Future;

pub struct User<Id>
where
    Id: Eq,
{
    id: Id,
}

pub trait DataProvider<Id>
where
    Id: Eq,
{
    type Error;

    fn get_user_info(&self, id: &Id) -> Result<User<Id>, Self::Error>;
}

#[cfg(feature = "async")]
#[async_trait]
pub trait AsyncDataProvider<Id>
where
    Id: Eq,
{
    type Error;

    async fn get_user_info(&self, id: &Id) -> Result<User<Id>, Self::Error>;
}
